//Все скрипты которые писали в рамках первого и второго задания - оформить в виде функций

//Задание 1_1
/*Переменная хранит в себе значение от 0 до 9. 
Написать скрипт который будет выводить слово “один”, 
если переменная хранит значение 1. Выводить слово “два” - 
если переменная хранит значение 2, и т.д. для всех цифр от 0 до 9. 
Реализовать двумя способами.*/
var num1 = 5;

function num_to_word(a){
    var res;
    m=["ноль","один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"];
    if ( (a>=0) && (a<10) && ((a-Math.trunc(a))==0)) {
        res = m[a];
    } else {
        res = "мне не нравится это значение переменной!";
    };
    return res

};
answer = num_to_word(num1);
console.log(answer);

//Задание 1_2
/*Переменная хранит в себе значение, напишите скрипт 
  который выводит информацию о том, что число является 
  нулевым, либо положительным, либо отрицательным.*/

var num="строка";

function type_of_number(a){

    if (a===0){
        b="число равно нулю";
    } else if(a>0) {
         b = "положительное";
    } else if(a<0) {
         b = "отрицательное";
    } else {
        b = "это не число!!";
    }
    return b
};

answer = type_of_number(num);
console.log(answer);


//Задание 1_3
/*Переменная хранит в себе единицу измерения одно из возможных 
 значений (Byte, KB, MB, GB), Вторая переменная хранит в себе целое число. 
 В зависимости от того какая единица измерения написать скрипт, который выводит 
 количество байт. Для вычисления принимает счет что в каждой последующей 
 единицы измерения хранится 1024 единиц более меньшего измерения.
 */


var edin = "MB";
var value = 3;
function perevod_edinic(a,b){
    
    switch(a){
        case("Byte"): res=b; break;
        case("KB"): res=b*1024; break;
        case("MB"): res=b*1024*1024; break;
        case("GB"): res=b*1024*1024*1024; break;
    };
 itog = res+" Byte";
return itog
};

answer = perevod_edinic(edin,value);
console.log(answer);

//Задание 1_4
/*Переменная хранит процент кредита, вторая переменная хранит объем тела кредита, 
третья переменная хранит длительность кредитного договора в годах. 
Написать скрипт который вычислит:
    Сколько процентов заплатит клиент за все время
    Сколько процентов заплатит клиент за один календарный год
    Какое общее количество денежных средств клиента банка выплатит за все года
*/
var percent = 10;
var body = 10000;
var years = 4;
var ostatok=body;
var first_year_perc=0;

console.log("Тип кредита - дифференцированный; процент начисляется на остаток от тела кредита")
console.log("Всего взяли в кредит сумму: "+body+" под "+percent+"% годовых")
var sum_perc=0, sum_plat=0; 



function credit(per, bod, ye){
    

for (i=1; i<=(ye*12); i++){
    per=percent*ostatok/(12*100); //процент по кредиту в этом месяце
    plat=bod/(ye*12)+per;  //расчет ежемесячного платежа
    //console.log(ostatok);
    
    sum_perc+=per;
    sum_plat+=plat;
    if(i==12){
        first_year_perc=sum_perc;
        //console.log(first_year_perc)
    }
    ostatok=ostatok-bod/(ye*12);
}
return [first_year_perc, sum_plat, sum_perc];

};
console.log(credit(percent,body,years))



//console.log("за первый календарный год клиент заплатит процентов банку: "+first_year_perc);
//console.log("за все время процентов банку клиент заплатит: "+sum_perc);
//console.log("всего денежных средств будет внесено клиентом: "+sum_plat);

//Задание 2_1

//Переменная содержит в себе строку. Вывести строку в обратном порядке.

var new_string='дезоксирибонуклеиновая кислота'

function inversia(st){

var invers="";
for(var i=(st.length-1); i>=0; i--){
   invers+=st[i];
}
return invers;
}
console.log('инверсия дезоксирибонуклеиновой кислоты')
console.log(inversia(new_string))

//Задание 2_2

//Переменная содержит в себе число. 
//Написать скрипт который посчитает факториал этого числа.

var a=4;
function factorial(num){

var fac=1;
for(var i=2; i<=a; i++){
    fac*=i;
};
return fac;
}
console.log('факториал от 4')
console.log(factorial(a))

//Задание 2_3

//Дано число - вывести первые N делителей этого числа нацело.

var a=222;
var N=5;

function getDelitili(a, N){
    let i=1;
    let res=[];

while(res.length<N){
    if((a % i)===0){
        
        res.push(i)
        
    }
    i++
}
return res;
}
console.log(N + ' делителей от числа '+a)
console.log(getDelitili(a,N))

//Задание 2_4

//Найти сумму цифр числа которые кратны двум

var a=242;

a=String(a)

function sumOfeven(a){

let sum=0;
for( var i=0; i<a.length; i++ ) {
    
    if((Number(a[i]) % 2)===0){
        sum += Number(a[i]);
    }
};
return sum;  
}
console.log('Сумма цифр числа ' + a + ', которые кратны двум');
console.log(sumOfeven(a));
 
//Задание 2_5

//Найти минимальное число которое больше 300 и нацело делиться на 17

var num=300;
var del = 17;

function minimDelitel(i, d){
var res=[]

while(res.length<1){
    if((i % d)===0){
        
        res.push(i)
        return i;
    }
    i++
}
return res;
}
console.log('Минимальное число, которое больше '+num+' и делится на '+ del);
console.log(minimDelitel(num,del));

//Задание 2_6

//Заданы две переменные для двух целых чисел, 
//найти максимальное общее значение которое нацело 
//делит два заданных числа.

var a=252;
var b=270;

function MaxObshDel(a,b){
var res;
if (a>b){
 var start = b;
} else { var start = a};

for(i=start;i>1;i--){
    if((a % i)===0){
        if((b % i)===0){
        res=i;
        return res;
        };
    };
        
    };
}
    console.log('Максимальный общий делитель от чисел '+ a+" и "+b);

console.log( MaxObshDel(a,b))





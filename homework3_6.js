//Задание 3_6
/*Удалить из массива все столбцы которые не имеют ни одного нулевого элемента и 
сумма которых положительна - оформить в виде функции*/

var l = 3;
var k = 4;


//Создаем такой рандом, который будет создавать как положительные, так и отрицательные целые числа
function getRandom(min, max) {
    return parseInt(Math.random() * (max - min) + min);
  }


  var min1=-8;
  var max1=9;

function create_matrix(str,stolb){
    var m = [];
    for(var i = 0; i < str; i++){
    m[i] = [];
    for(var j = 0; j < stolb; j++){
        m[i][j] = getRandom(min1,max1);
    }
};
return m;
}
var A = create_matrix(l,k);
var B=A;
console.log('Исходная матрица');
console.log(B);



function obrezanie(m){
    
    for (var i=0; i<m[0].length; i++){
        
        sum=0;
        var status='danger';
        for(var j = 0; j < m.length; j++){
            if (m[j][i]==0){
                status = 'good'; break;
            } 
                sum+=m[j][i];
            }
        
        if ((status == 'danger') && (sum > 0)){
            for(var j = 0; j < m.length; j++){
               m[j].splice(i, 1);
            }
            
        }
        
    }
    return m;
}
console.log('Результат после удаления столбцов');
console.log(obrezanie(A))
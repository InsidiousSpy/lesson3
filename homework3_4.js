//Задание 3_4

/*
Написать функцию, которая складывает две матрицы
*/

//сначала создаем матрицы



var l = 5;
var k = 7;


function create_matrix(str,stolb){
    var m = [];
    for(var i = 0; i < str; i++){
    m[i] = [];
    for(var j = 0; j < stolb; j++){
        m[i][j] = parseInt(Math.random() * 12);
    }
};
return m;
}
var A = create_matrix(l,k);
var B = create_matrix(l,k);
console.log('первая матрица:')
console.log(A);
console.log('вторая матрица:')
console.log(B);

//теперь складываем их поэлементно



function matr_sum(m,n){
    var c=[];
    for (var i=0; i<m.length; i++){
        c[i]=[];
        for(var j = 0; j < m[0].length; j++){
            c[i][j]=m[i][j]+n[i][j];
            
        }
    }
   return c;
   };
   console.log('результат суммирования:')
   console.log(matr_sum(A,B));


var D = create_matrix(l,k);
var F = create_matrix(k,l);
console.log('третья матрица:')
console.log(D);
console.log('четвертая матрица:')
console.log(F);

   function matr_mult(m,n){
    var c=[];
    for (var i=0; i<m.length; i++){
        c[i]=[];
        for (var k=0; k<m.length; k++){

            var elem=0;
            for (var j=0; j<m[0].length; j++){
                elem+=m[i][j]*n[j][k];
            }
            c[i][k]=elem;
        }
    }
   return c;
   };
   console.log('результат умножения:')
   console.log(matr_mult(D,F));


//Все скрипты которые используют в своей основе цикл - написать с помощью рекурсивных функций


//Задание 1_4
/*Переменная хранит процент кредита, вторая переменная хранит объем тела кредита, 
третья переменная хранит длительность кредитного договора в годах. 
Написать скрипт который вычислит:
    Сколько процентов заплатит клиент за все время
    Сколько процентов заплатит клиент за один календарный год
    Какое общее количество денежных средств клиента банка выплатит за все года
*/

var percent = 10;
var body = 10000;
var years = 4;
var ostatok=body;
var first_year_perc=0;

console.log("Тип кредита - дифференцированный; процент начисляется на остаток от тела кредита")
console.log("Всего взяли в кредит сумму: "+body+" под "+percent+"% годовых")



function credit_rec(bod, i){
    var sum_perc=0, sum_plat=0; 
    if (i<=(years*12)) {
        per=percent*bod/(12*100); //процент по кредиту в этом месяце
        plat=body/(years*12)+per;  //расчет ежемесячного платежа
        
        bod += -body/(years*12);
        sum_perc+=per;
        sum_plat+=plat;
        if(i==12){
            first_year_perc=sum_perc;
            
        }

        i++;
        return credit_rec(bod, i)
    } else  {
        return [first_year_perc, sum_plat, sum_perc];
    }

    }

    console.log(credit_rec(body,i))

   




//Задание 2_1

//Переменная содержит в себе строку. Вывести строку в обратном порядке.

var st='дезоксирибонуклеиновая кислота';
var i = (st.length-1);
var res='';
function inverse(st,i){

    if (i>=0) {  
        res+=st[i];
        --i;
        inverse(st, i);
    }
    
    return res;
}

console.log(inverse(st,i))

 
//Задание 2_2

//Переменная содержит в себе число. 
//Написать скрипт который посчитает факториал этого числа.

var a=4;
var res = 1;
function factorial(a){
if (a>1){
    res*=a;
    a--
    factorial(a)
}
return res;
}
console.log(factorial(a))



//Задание 2_3

//Дано число - вывести первые N делителей этого числа нацело.

var a=222;
var N=5;
var res=[]
var i=1;


function getDeliteli(a, N, res){
    if (res.length<N){
        if ((a % i)===0){
            res.push(i) 
        }
        i++
        getDeliteli(a, N, res)
    }
    return res;
    }


console.log(getDeliteli(a, N, res))



/*
//Задание 2_4

//Найти сумму цифр числа которые кратны двум

var A=2432;
var sum=0;
var i=0;


//console.log(String(a).length)
function getSumCratnoe(a,sum){
    a=String(a);
    if (i < a.length ){
        if((Number(a[i]) % 2)===0){
            sum += Number(a[i]);
            console.log([i,a[i],sum]);
            getSumCratnoe(a,sum);
        }
        i++ 
    }
    
    
    console.log([i,a,sum]);
    return sum
    

}

console.log( getSumCratnoe(A,sum) )

var a=2432;
var sum=0;
var i=0;
a=String(a);

for( var i=0; i<a.length; i++ ) {
    
    if((Number(a[i]) % 2)===0){
        sum += Number(a[i]);
    }
};  

console.log(sum);

*/
 
 
//Задание 2_5

//Найти минимальное число которое больше 300 и нацело делиться на 17


var res=[]
var i=300;
res=[];

function minChislo(i, res){
    if (res.length<1){
    if((i % 17)===0){
        res.push(i)
    }
    i++
    minChislo(i, res)
}
return res[0]
}
console.log(minChislo(i, res))










